<?php

namespace TheFeed\Controleur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\Conteneur;

class RouteurURL
{
    public static function traiterRequete() {

        $requete = Request::createFromGlobals();
        $routes = new RouteCollection();

// Route feed
        $route = new Route("/", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::feed",
        ]);
        $routes->add("feed", $route);


        // Route afficherFormulaireConnexion
        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireConnexion",
            // Syntaxes équivalentes
            // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
            // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],
        ]);
        $route->setMethods(["GET"]);
        $routes->add("afficherFormulaireConnexion", $route);

        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::connecter",
            // Syntaxes équivalentes
            // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
            // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],
        ]);
        $route->setMethods(["GET"]);
        $routes->add("connecter", $route);


        $route = new Route("/deconnexion", [
            "_controller" => ControleurUtilisateur::class . "::deconnecter",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("deconnecter", $route);



        $route = new Route("/feedy", [
            "_controller" => ControleurPublication::class . "::submitFeedy",
        ]);
        $route->setMethods(["POST"]);
        $routes->add("feeedy", $route);


        $route = new Route("/inscription", [
            "_controller" => ControleurUtilisateur::class . "::afficherFormulaireCreation",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("inscription", $route);


        $route = new Route("/inscription", [
            "_controller" => ControleurUtilisateur::class . "::creerDepuisFormulaire",
        ]);
        $route->setMethods(["POST"]);
        $routes->add("creerDepuisFormulaire", $route);


        $route = new Route("/utilisateur/{idUser}", [
            "_controller" => ControleurUtilisateur::class . "::pagePerso",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("pagePerso", $route);



        $contexteRequete = (new RequestContext())->fromRequest($requete);
        $associateurUrl = new UrlMatcher($routes, $contexteRequete);
        $donneesRoute = $associateurUrl->match($requete->getPathInfo());
        $requete->attributes->add($donneesRoute);

        $resolveurDeControleur = new ControllerResolver();
        $controleur = $resolveurDeControleur->getController($requete);

        $resolveurDArguments = new ArgumentResolver();
        $arguments = $resolveurDArguments->getArguments($requete, $controleur);


        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);
        $generateurUrl = new UrlGenerator($routes, $contexteRequete);

        Conteneur::ajouterService("service",$assistantUrl);
        Conteneur::ajouterService("generateur",$generateurUrl);

        call_user_func_array($controleur, $arguments);



    }
}