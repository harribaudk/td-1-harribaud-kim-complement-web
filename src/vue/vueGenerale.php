<?php

use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;

$assistant = Conteneur::recupererService("service");
$generateur = Conteneur::recupererService("generateur");

/**
 * @var string $pagetitle
 * @var string $cheminVueBody
 * @var String[][] $messagesFlash
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?= $pagetitle ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href=<?= htmlspecialchars($assistant->getAbsoluteURL("assets/css/styles.css")); ?>>
</head>

<body>
    <header>
        <div id="titre" class="center">
            <a href=<?= rawurlencode($generateur->generate("feed")); ?>><span>The Feed</span></a>
            <nav>
                <a href=<?= rawurlencode($generateur->generate("feed")) ?>>Accueil</a>
                <?php
                if (!ConnexionUtilisateur::estConnecte()) {
                ?>
                    <a href=<?= rawurlencode($generateur->generate("inscription")); ?>>Inscription</a>
                    <a href=<?= rawurlencode($generateur->generate("connecter")); ?>>Connexion</a>
                <?php
                } else {
                    $idUtilisateurURL = rawurlencode(ConnexionUtilisateur::getIdUtilisateurConnecte());
                ?>
                    <a href=<?= rawurlencode($generateur->generate("pagePerso",["idUser" => $idUtilisateurURL])) ?>>Ma
                        page</a>
                    <a href=<?= rawurlencode($generateur->generate("deconnexion")); ?>>Déconnexion</a>
                <?php } ?>
            </nav>
        </div>
    </header>
    <div id="flashes-container">
        <?php
        foreach (["success", "error"] as $type) {
            foreach ($messagesFlash[$type] as $messageFlash) {
        ?>
                <span class="flashes flashes-<?= $type ?>"><?= $messageFlash ?></span>
        <?php
            }
        }
        ?>
    </div>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</body>

</html>